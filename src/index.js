import React,{Component} from 'react';
import {render} from 'react-dom';

import './normalize.scss';

import Cabecera from './componentes/Cabecera';
import Heroe from './componentes/Heroe';
import QuienesSomos from './componentes/QuienesSomos';
import NuestrosServicios from './componentes/NuestrosServicios';
import Portfolio from './componentes/Portfolio';
import Contacto from './componentes/Contacto';
import PiePagina from './componentes/PiePagina';

class App extends Component{
	constructor(props){
		super(props);
	}
	render() {
		return (
			<div>
				<Cabecera />
				<Heroe />
				<QuienesSomos />
				<NuestrosServicios />
				<Portfolio />
				<Contacto />
				<PiePagina />
			</div>
		);
	}
}

render(<App />,document.getElementById('root'));