
import React,{ Component } from 'react';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faDotCircle} from '@fortawesome/free-regular-svg-icons';
import { faFacebookF,faInstagram } from '@fortawesome/free-brands-svg-icons' ;

library.add(faFacebookF,faInstagram,faDotCircle);

import style from './style.scss';

import logo from '../../assets/logoB.png';

class PiePagina extends Component{
    constructor(props){
        super(props);
        this.state ={
            menu: [
                {url:'/#home',texto:'HOME'},
                {url:'/#quienessomos',texto:'QUIENES SOMOS'},
                {url:'/#servicios',texto:'SERVICIOS'},
                {url:'/#portfolio',texto:'PORTFOLIO'},
                {url:'/#contacto',texto:'CONTACTO'},
            ]
        }
    }
    render(){
        return (
            <div className={style.piePagina}>
                <div className={style.mobile}>
                    <img src={logo}/>
                </div>
                <div className={style.direcciones}>
                    <div className={style.titulo}><img src={logo}/></div>
                    <ul>
                        <li><a href='#'><FontAwesomeIcon icon={['far', 'dot-circle']} /> Mallorca, ISLAS BALEARES </a></li>
                        <li><a href='#'><FontAwesomeIcon icon={['fab', 'facebook-f']} /> @photozpot </a></li>
                        <li><a href='#'><FontAwesomeIcon icon={['fab', 'instagram']} /> @photozpot </a></li>
                    </ul>
                </div>
                <div className={style.navegacion}>
                    <div className={style.titulo}><h2>Navegacion</h2></div>
                    <ul>
                        {
                            this.state.menu.map((item,key)=>{
                                return <li key={key}><a href={item.url}>{item.texto}</a></li>
                            })
                        }
                    </ul>
                </div>
                <div className={style.redes}>
                    <div className={style.titulo}><h2>Contactate a:</h2></div>
                        <ul>
                            <li><a href='#'><FontAwesomeIcon icon={['far', 'dot-circle']} /> info@photozpot.com </a></li>
                            <li><a href='#'><FontAwesomeIcon icon={['far', 'dot-circle']} /> Tel +(34)680324768 </a></li>
                        </ul>
                </div>
                <div className={style.copyright}>2019 power by LUKE</div>
            </div>
        );
    }
}

export default PiePagina;