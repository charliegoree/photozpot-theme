import React,{Component} from 'react';

import style from './style.scss';

import lupa from '../../assets/lupa.png';
import libro from '../../assets/libro.png';
import camara from '../../assets/camara.png';
import megafono from '../../assets/megafono.png';
import pc from '../../assets/pc.png';
import cruz from '../../assets/cruz2.png';
import fondo from '../../assets/fondo.png';

class NuestrosServicios extends Component{
    constructor(props){
        super(props);
        this.state = {
            items : [
                {img:lupa,titulo:'Gestión de Dominios',texto:'Tu Sitio web comienza con el dominio perfecto'},
                {img:libro,titulo:'Gestión Registro Marca',texto:'Rápido, Sencillo y Económico'},
                {img:camara,titulo:'Fotografía de Productos',texto:'Somos expertos en fotografía de producto'},
                {img:megafono,titulo:'Diseño y Comunicación',texto:'Diseño Gráfico y Publicitario'},
                {img:cruz,titulo:'Diseño Productos Digitales',texto:'Investigamos, diseñamos y creamos tu producto digital'},
                {img:pc,titulo:'Redes Sociales',texto:'Planificar, redactar, diseñar y monitorear nuestra estrategia digital'}
            ]
        }
    }
    render(){
        return(
            <div className={style.servicios} id="servicios">
                <h1>Nuestros Servicios</h1>
                <div className={style.contenedor}>
                    <div className={style.columna1}>
                        <h1>Desarrollamos<br/>estrategias<br/>únicas</h1>
                        <p>Personalizamos cada una de nuestras estrategias de comunicación, adaptándolas en todos los medios.</p>
                        <a href="/#portfolio">Conoce Más</a>
                    </div>
                    <div className={style.columna2}>
                        {
                            this.state.items.map((item,key)=>{
                                return (
                                    <div className={style.item} key={key}>
                                         <img src={item.img}/>
                                        <h3>{item.titulo}</h3>
                                        <p>{item.texto}</p>
                                    </div>
                                    );
                            })
                        }
                    </div>
                </div>
                <img src={fondo} className={style.fondo}/>
            </div>
        );
    }
}

export default NuestrosServicios;