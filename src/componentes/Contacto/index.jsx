
import React,{ Component } from 'react';
import * as emailjs from "emailjs-com";

import style from './style.scss';

class Contacto extends Component{
    constructor(props){
        super(props);
        this.state = {
			nombre:"",
			apellido:"",
			email:"",
			mensaje:""
		}
		this.cambiarNombre = this.cambiarNombre.bind(this);
		this.cambiarApellido = this.cambiarApellido.bind(this);
		this.cambiarEmail = this.cambiarEmail.bind(this);
		this.cambiarMensaje = this.cambiarMensaje.bind(this);
		this.enviarMensaje = this.enviarMensaje.bind(this);
    }
    componentDidMount(){
		emailjs.init("user_qTiPG8VwKYDUEVFTadxLY");
	}
	cambiarNombre(event){
		let value = event.target.value;
		this.setState({nombre:value});
	}
	cambiarApellido(event){
		let value = event.target.value;
		this.setState({apellido:value});
	}
	cambiarEmail(event){
		let value = event.target.value;
		this.setState({email:value});
	}
	cambiarMensaje(event){
		let value = event.target.value;
		this.setState({mensaje:value});
    }
    enviarMensaje(event){
		emailjs.send('mailjet', 'template_Tir3wPI6', {nombre:this.state.nombre,apellido:this.state.apellido,email:this.state.email,mensaje:this.state.mensaje})
	    .then(function(response) {
	       console.log('SUCCESS!', response.status, response.text);
	    }, function(error) {
	       console.log('FAILED...', error);
	    });
	}
    render(){
        return(
            <div className={style.contacto} id="contacto">
                <h1>Contactanos</h1>
                <p>¡Si tienes una idea o un proyecto no dudes en contactarnos!</p>
                <div className={style.contenido}>
					<form onSubmit={this.enviarMensaje}>
						<div className={style.filaDoble}>
							<input type="text" placeholder="Nombre" value={this.state.nombre} onChange={this.cambiarNombre} required/>
							<input type="text" placeholder="Apellido" value={this.state.apellido} onChange={this.cambiarApellido} required/>
						</div>
						<div className={style.fila}>
							<input type="text" placeholder="Dirección de Correo Electrónico" value={this.state.email} onChange={this.cambiarEmail} required/>
						</div>
						<div className={style.fila}>
							<textarea placeholder="Mensaje" value={this.state.mensaje} onChange={this.cambiarMensaje} ></textarea>
						</div>
						<div className={style.fila}>
							<button type="submit">Enviar Mensaje</button>
						</div>
					</form>
                </div>
            </div>
        );
    }
}

export default Contacto;