
import React,{ Component } from 'react';

import style from './style.scss';

import img1 from '../../assets/imagenes/01.png';
import img2 from '../../assets/imagenes/02.png';
import img3 from '../../assets/imagenes/03.png';
import img4 from '../../assets/imagenes/04.png';
import img5 from '../../assets/imagenes/05.png';
import img6 from '../../assets/imagenes/06.png';
import img7 from '../../assets/imagenes/07.png';
import img8 from '../../assets/imagenes/08.png';

class Portfolio extends Component{
    constructor(props){
        super(props);
        this.state = {
            seleccionada:'',
            abierto: false
        }
        this.modal = this.modal.bind(this);
    }
    modal(img){
        console.log('apreto');
        this.setState({
            seleccionada: img,
            abierto: !this.state.abierto
        });
    }
    render(){
        return(
            <div className={style.portfolio} id="portfolio">
                <h1>Portfolio</h1>
                <div className={style.contenedor}>
                    <div className={style.itemA} onClick={()=>this.modal(img4)}>
                        <img src={img4}/>
                    </div>
                    <div className={style.itemB} onClick={()=>this.modal(img1)}>
                        <img src={img1}/>
                    </div>
                    <div className={style.itemC} onClick={()=>this.modal(img6)}>
                        <img src={img6}/>
                    </div>
                    <div className={style.itemD} onClick={()=>this.modal(img3)}>
                        <img src={img3}/>
                    </div>
                    <div className={style.itemE} onClick={()=>this.modal(img5)}>
                        <img src={img5}/>
                    </div>
                    <div className={style.itemF} onClick={()=>this.modal(img2)}>
                        <img src={img2}/>
                    </div>
                    <div className={style.itemG} onClick={()=>this.modal(img7)}>
                        <img src={img7}/>
                    </div>
                    <div className={style.itemH} onClick={()=>this.modal(img8)}>
                        <img src={img8}/>
                    </div>
                </div>
                <div className={style.modal} onClick={()=>this.modal('')} style={{display:(this.state.abierto ? 'flex' : 'none')}}>
                    <img src={this.state.seleccionada} />
                </div>
            </div>
        );
    }
}

export default Portfolio;