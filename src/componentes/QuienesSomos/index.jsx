import React,{Component} from 'react';

import style from './style.scss';

import lupa from '../../assets/lupa.png';
import cruz from '../../assets/cruz1.png';
import avion from '../../assets/avion.png';

class QuienesSomos extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className={style.somos} id="quienessomos">
                <h1>Quienes Somos</h1>
                <div className={style.contenedor}>
                    <div className={style.item}>
                        <img src={lupa} alt="Lupa"/>
                        <h2>Generamos una idea</h2>
                        <p>Un concepto que pueda ser implementado para la unidad de negocio de tu marca</p>
                    </div>
                    <div className={style.item}>
                        <img src={cruz} alt="Cruz con lapiz y pluma."/>
                        <h2>Ejecutas y diseñamos</h2>
                        <p>Ejecutamos nuestras ideas y las diseñamos para que tu marca enamore a los potenciales clientes</p>
                    </div>
                    <div className={style.item}>
                        <img src={avion} alt="Avion de papel"/>
                        <h2>Potenciamos</h2>
                        <p>Potenciar esos atributos para hacer que tu marca pueda llegar realmente al público al que te diriges.</p>
                    </div>
                </div>
                <div className={style.fondo}></div>
            </div>
        );
    }
}

export default QuienesSomos;