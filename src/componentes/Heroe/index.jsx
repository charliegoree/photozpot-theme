import React,{Component } from 'react';

import style from './style.scss';

import dibujo from '../../assets/hero.png';

class Heroe extends Component{
    render(){
        return(
            <div className={style.heroe}>
                <div className={style.contenedor}>
                    <div className={style.mitad1}>
                        <h1>Comunicación 360º para tu empresa</h1>
                        <p>Hola, somos PhotoZpot! una agencia enfocada en generar, ejecutar, diseñar e implementar comunicación en todos sus niveles.</p>
                        <div className={style.botonera}>
                            <a href="/#quienessomos">Conocer Más</a>
                            <a href="/#contacto">Contactanos</a>
                        </div>
                    </div>
                    <div className={style.mitad2}>
                        <img src={dibujo} alt="Dibujo"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Heroe;