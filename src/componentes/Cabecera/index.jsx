import React,{Component} from 'react';


import style from './style.scss';

import logo from '../../assets/logoN.png';

class Cabecera extends Component{
    constructor(props){
        super(props);
        this.state ={
            menu: [
                {url:'/#home',texto:'HOME'},
                {url:'/#quienessomos',texto:'QUIENES SOMOS'},
                {url:'/#servicios',texto:'SERVICIOS'},
                {url:'/#portfolio',texto:'PORTFOLIO'},
                {url:'/#contacto',texto:'CONTACTO'},
            ]
        }
    }
    render(){
        return (
            <header className={style.cabecera} id="home">
                <div className={style.contenedor}>
                    <div className={style.logo}>
                        <img src={logo} alt="PhotoZpot"/>
                    </div>
                    <nav>
                        <ul>
                            {
                                this.state.menu.map((item,key)=>{
                                    return <li key={key}><a href={item.url}>{item.texto}</a></li>
                                })
                            }
                        </ul>
                    </nav>
                </div>
            </header>
        );
    }
}

export default Cabecera;